#!/bin/bash
# Get stype of install
echo "Please select the type of server (standalone|www|db):"
read SERVERTYPE

# Install updates
sudo apt-get update -y && sudo apt-get upgrade -y

# Install unattended-upgrades
sudo apt install unattended-upgrades -y
sudo apt install apticron -y
echo "EMAIL=\"webmaster@managedword.com\"" >> /etc/apticron/apticron.conf

# Install Git if needed
sudo apt-get install git -y

# Install monit
sudo apt-get install monit -y

# Set up firewall
sudo ufw allow OpenSSH
sudo ufw allow mysql
sudo ufw allow proto tcp from any to any port 80,443,22222,2812
sudo ufw enable

# Create a webmaster user and set up authentication
adduser webmaster
usermod -aG sudo webmaster

# Add authorized keys to webmaster
mkdir -p /home/webmaster/.ssh
cp ~/.ssh/authorized_keys /home/webmaster/.ssh/authorized_keys
chown -R webmaster:webmaster /home/webmaster/.ssh
chmod 700 /home/webmaster/.ssh
chmod 600 /home/webmaster/.ssh/authorized_keys

# Toggle PasswordAuthentication to no and reload SSHD
sed -re 's/^(PasswordAuthentication)([[:space:]]+)yes/\1\2no/' -i.`date -I` /etc/ssh/sshd_config
sudo systemctl reload sshd

# Set up Git user information
git config --global user.name "Webmaster"
git config --global user.email webmaster@managedword.com

# Install EasyEngine
wget -qO ee rt.cx/ee && sudo bash ee

# Install Monit
sudo apt-get install monit -y

# Format and mount block storage
BLOCK="false"
for FILENAME in /dev/disk/by-id/*DO_Volume*; do
  BLOCK="true"
  sudo mkfs.ext4 -F $FILENAME
  sudo mkdir -p /mnt/data
  sudo mount -o discard,defaults $FILENAME /mnt/data;
  echo $FILENAME /mnt/data ext4 defaults,nofail,discard 0 0 | sudo tee -a /etc/fstab
done

if [ "$SERVERTYPE" == "standalone" ]
  then
    if [ "$BLOCK" = "true" ]; then
      sudo mkdir -p /mnt/data/www
      sudo ln -nsf /mnt/data/www /var/www
    fi
    sudo ee stack install
    sudo wget -qO /etc/monit/monitrc https://bitbucket.org/managedword/scripts/raw/master/monit/standalone
fi

if [ "$SERVERTYPE" == "www" ]
  then
    if [ "$BLOCK" = "true" ]; then
      sudo mkdir -p /mnt/data/www
      sudo ln -nsf /mnt/data/www /var/www
    fi
    sudo ee stack install
    sudo ee stack remove --mysql
    sudo apt-get install mysql-client -y
    sudo wget -qO /etc/monit/monitrc https://bitbucket.org/managedword/scripts/raw/master/monit/www
fi

if [ "$SERVERTYPE" == "db" ]
  then
    if [ "$BLOCK" = "true" ]; then
      sudo mkdir /mnt/data/mysql
      sudo ln -nsf /mnt/data/mysql /var/lib/mysql
    fi
    sudo ee stack install --mysql
    sudo wget -qO /etc/monit/monitrc https://bitbucket.org/managedword/scripts/raw/master/monit/db
fi

# Restart services
sudo service monit restart
