#!/bin/bash
echo "Please specify the site ID you would like to export"
read WEBDOMAIN
echo "What domain would you like to back up?"
read WEBDOMAIN

cd /var/www/$WEBDOMAIN/htdocs
if [ -f "/var/www/$WEBDOMAIN/htdocs/db.sql" ]; then sudo rm db.sql; fi
sudo -u www-data wp db export db.sql
if [ -f "/var/www/$WEBDOMAIN/htdocs/$WEBDOMAIN.tar.gz" ]; then sudo rm /var/www/$WEBDOMAIN/htdocs/$WEBDOMAIN.tar.gz; fi
sudo -u www-data tar czf $WEBDOMAIN.tar.gz db.sql ./wp-content/plugins ./wp-content/themes ./wp-content/uploads
sudo rm db.sql
