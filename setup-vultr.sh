#!/bin/bash

# Set the server type: standalone, www, or db
if [ "$1" == "" ]
  then
    echo "Please select the type of server you would like to initialize (standalone,www,db):"
    read servertype
  else
    servertype=$1
fi

# Get the internal IPs for the server and sibling
if [ "$servertype" == "www" ] || [ "$servertype" == "db" ]
  then

    # If not provided, set the internal IP
    if [ "$2" == "" ]
      then
        echo "What is this server's internal IP address:"
        read internalip
      else
        internalip=$2
    fi

    # If not provided, set the sibling IP
    if [ "$3" == "" ]
      then
        echo "What is this server's sibling IP address:"
        read siblingip
      else
        siblingip=$3
    fi
fi

sudo apt-get update -y && sudo apt-get upgrade -y && sudo apt-get dist-upgrade -y

if [ -b /dev/vdb ]
  then
    # (Linux Example) Create partitions:
    sudo parted -s /dev/vdb mklabel gpt
    sudo parted -s /dev/vdb unit mib mkpart primary 0% 100%

    # (Linux Example) Create filesystem:
    sudo mkfs.ext4 /dev/vdb1

    # (Linux Example) Mount block storage:
    sudo mkdir /mnt/blockstorage
    sudo echo >> /etc/fstab
    sudo echo /dev/vdb1               /mnt/blockstorage       ext4    defaults,noatime 0 0 >> /etc/fstab
    sudo mount /mnt/blockstorage

    # Check for shared server config and link external mounts
    if [ "$servertype" = "www" ]
      then
        sudo mkdir /mnt/blockstorage/www
        sudo ln -nsf /mnt/blockstorage/www /var/www
    fi

    if [ "$servertype" = "db" ]
      then
        sudo mkdir /mnt/blockstorage/mysql
        sudo ln -nsf /mnt/blockstorage/mysql /var/lib/mysql
    fi
fi

# Set up secondary IP
if [ "$internalip" != "" ]
  then
    sudo echo -e "auto eth1\niface eth1 inet static\n    address $internalip\n    netmask 255.255.0.0\n            mtu 1450" >> /etc/network/interfaces
    sudo ifup eth1
fi

# Set up Git user information
sudo apt-get install git -y
git config --global user.name "Webmaster"
git config --global user.email webmaster@managedword.com

# Install EasyEngine
wget -qO ee rt.cx/ee && sudo bash ee

# Install Monit
sudo apt-get install monit -y

# Standalone server initialization
if [ "$servertype" == "standalone" ]
  then
    ee stack install
    wget -qO /etc/monit/monitrc https://bitbucket.org/managedword/scripts/raw/master/monit/standalone
fi

# WWW server initialization
if [ "$servertype" == "www" ]
  then
    ee stack install
    ee stack remove --mysql
    sudo apt-get install mysql-client -y
    wget -qO /etc/monit/monitrc https://bitbucket.org/managedword/scripts/raw/master/monit/www
fi

# DB server initialization
if [ "$servertype" == "db" ]
  then
    ee stack install --mysql
    wget -qO /etc/monit/monitrc https://bitbucket.org/managedword/scripts/raw/master/monit/db
fi

# Restart services
sudo service monit restart
