#!/bin/bash
echo "Domain name of the site you are restoring:"
read WEBDOMAIN

echo "URL for backup files:"
read BACKUPS

echo "Would you like to set up a new instance with Easy Engine? (Y/n)"
read NEWINSTALL

# Create a new website instance if required
if [ "$NEWINSTALL" == "y" ] || [ "$NEWINSTALL" == "" ]
  then
    sudo ee site create $WEBDOMAIN --wpfc
fi

# Remove any old restore folders
cd /var/www/$WEBDOMAIN/
if [ -f "/var/www/$WEBDOMAIN/restore" ]
  then
    sudo rm -Rf restore
fi

# Create a restore folder under site root
sudo -u www-data mkdir restore
cd restore

# Download the backup file from the original website, if available
sudo -u www-data wget $BACKUPS
sudo -u www-data tar xf $WEBDOMAIN.tar.gz

# Rsync the downloaded files to the current website
sudo -u www-data rsync -aqP /var/www/$WEBDOMAIN/restore/wp-content/plugins/ /var/www/$WEBDOMAIN/htdocs/wp-content/plugins/
sudo -u www-data rsync -aqP /var/www/$WEBDOMAIN/restore/wp-content/themes/ /var/www/$WEBDOMAIN/htdocs/wp-content/themes/
sudo -u www-data rsync -aqP /var/www/$WEBDOMAIN/restore/wp-content/uploads/ /var/www/$WEBDOMAIN/htdocs/wp-content/uploads/

# Use WP CLI to restore the original database
cd /var/www/$WEBDOMAIN/htdocs
sudo -u www-data wp db import /var/www/$WEBDOMAIN/restore/db.sql

# Clean up the restore folder
sudo -u www-data rm -Rf /var/www/$WEBDOMAIN/restore/
