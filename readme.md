# Managed Word Server Initialization Scripts

Scripts for setting up and updating Managed Word servers.

## Usage

### Installing new servers

`wget -qO setup-aws.sh https://bitbucket.org/managedword/scripts/raw/master/setup-aws.sh && sudo bash setup-aws.sh`

or

`wget -qO setup-aws-webinoly.sh https://bitbucket.org/managedword/scripts/raw/master/setup-aws-webinoly.sh && sudo bash setup-aws-webinoly.sh`

You will be asked for the following information
* Type of server

If the system isn't a standalone server, you will still need to set up MySQL to accept outside

### Backing up and restoring Easy Engine sites

Because Easy Engine installs everything in the same place with the same naming structure, all you really need to create a full backup of a site is the domain name. This script will ask for that and then attempt to back up the database, plugins, themes, and uploads. This creates a fairly clean set of files to import. Requires root access as it saves files as www-data.

`wget -qO backup-ee-single.sh https://bitbucket.org/managedword/scripts/raw/master/backup-ee-single.sh && sudo bash backup-ee-single.sh`

Restore is designed to work with sites that do not already have DNS pointed to the new host and have already backed up using the script above. It can create a new Easy Engine site instance if one does not already exist on the host.

`wget -qO restore-ee-single.sh https://bitbucket.org/managedword/scripts/raw/master/restore-ee-single.sh && sudo bash restore-ee-single.sh`

## Contributing

1. Fork it!
2. Create your feature branch: `git checkout -b my-new-feature`
3. Commit your changes: `git commit -am 'Add some feature'`
4. Push to the branch: `git push origin my-new-feature`
5. Submit a pull request :D

## History

TODO: Write history

## Credits

TODO: Write credits

## License

TODO: Write license
