#!/bin/bash
# Get stype of install
echo "Please select the type of server (standalone|www|db):"
read SERVERTYPE

# Install updates
sudo apt-get update -y && sudo apt-get upgrade -y

# Install unattended-upgrades
sudo apt install unattended-upgrades -y
sudo apt install apticron -y
echo "EMAIL=\"webmaster@managedword.com\"" >> /etc/apticron/apticron.conf

# Install Git if needed
sudo apt-get install git -y

# Set up Git user information
git config --global user.name "Webmaster"
git config --global user.email webmaster@managedword.com

# Install EasyEngine
wget -qO ee rt.cx/ee && sudo bash ee

# Install monitoring
sudo apt-get install monit clamav htop -y

if [ "$SERVERTYPE" == "standalone" ]
  then
    sudo ee stack install
    sudo wget -qO /etc/monit/monitrc https://bitbucket.org/managedword/scripts/raw/master/monit/standalone
fi

if [ "$SERVERTYPE" == "www" ]
  then
    sudo ee stack install
    sudo ee stack remove --mysql
    sudo apt-get install mysql-client -y
    sudo wget -qO /etc/monit/monitrc https://bitbucket.org/managedword/scripts/raw/master/monit/www
fi

if [ "$SERVERTYPE" == "db" ]
  then
    sudo ee stack install --mysql
    sudo wget -qO /etc/monit/monitrc https://bitbucket.org/managedword/scripts/raw/master/monit/db
fi

# Restart services
sudo service monit restart
